#!/bin/sh
THISDIR="$( dirname $0 )"
apt-get update > /dev/null 2>&1
apt-get install puppet -y || apt-get install puppet-agent -y || yum install -y --nogpgcheck puppet || yum install -y --nogpgcheck puppet-agent

puppet apply install.pp -l $THISDIR/install.log --modulepath=$THISDIR/modules
