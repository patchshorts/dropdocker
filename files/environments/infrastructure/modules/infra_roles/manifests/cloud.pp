# Class: infra_roles::cloud
#
# Puppermaster, libvirt, jenkins server
# Contains:
#   - Virtual Machine Host
#   - Puppetmaster configuration
#   - Jenkins Server
#
class infra_roles::cloud inherits infra_roles {
  include infra_profiles::docker
  include infra_profiles::puppetmaster
  include infra_profiles::jenkins
  include infra_profiles::aptmirror
}
