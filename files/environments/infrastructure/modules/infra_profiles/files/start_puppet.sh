#!/bin/bash
# Usage start_puppet.sh 'trollbridge' 'QA' 'US' 'cloud.austin.rr.com' '15m' 'web' 'puppet arguments' 'xenial/zesty'
echo "Launching docker host puppet container with args: app: ${1} env: ${2} country: ${3} server: ${4} int:${5} role:${6} puppetargs:${7} ubuntutag:${8}"
echo "Installing Facts"
mkdir -p /etc/facter/facts.d
echo "#!/bin/sh" > /etc/facter/facts.d/server_id.sh
echo "echo 'applicationname=${1}'" >> /etc/facter/facts.d/server_id.sh
echo "echo 'appenv=${2}'" >> /etc/facter/facts.d/server_id.sh
echo "echo 'country=${3}'" >> /etc/facter/facts.d/server_id.sh
echo "echo 'role=${6}'" >> /etc/facter/facts.d/server_id.sh
echo "echo 'puppetmaster=${4}'" >> /etc/facter/facts.d/server_id.sh
echo "echo 'ubuntutag=${8}'" >> /etc/facter/facts.d/server_id.sh
chmod 755 /etc/facter/facts.d/server_id.sh
echo "Creating Symlinks"
ln -s /etc/puppetlabs/puppet/ /etc/puppet
ln -s /opt/puppetlabs/puppet /var/lib/puppet
mkdir -p /etc/puppetlabs/puppet/
echo "Installing puppet.conf"
echo "[main]" > /etc/puppet/puppet.conf
echo "environment = ${2}" >> /etc/puppet/puppet.conf
echo "environmentpath = /etc/puppet/environments" >> /etc/puppet/puppet.conf
echo "certname = `hostname -f`" >> /etc/puppet/puppet.conf
echo "server = ${4}" >> /etc/puppet/puppet.conf
echo "runinterval = ${5}" >> /etc/puppet/puppet.conf
echo "strict_variables = true" >> /etc/puppet/puppet.conf
echo "ssldir = /var/lib/puppet/ssl" >> /etc/puppet/puppet.conf
ln -s /opt/puppetlabs/bin/puppet /bin/puppet
echo "Launching agent firstrun."
sed -e "s#archive\.ubuntu\.com#${4}#g" -i /etc/apt/sources.list
sed -e "s#security\.ubuntu\.com#${4}#g" -i /etc/apt/sources.list
apt-get update
puppet agent -t -w 3600 $7
service puppet-agent start
echo "Running"
while /bin/true
do
	/bin/echo -n "."
	sleep 60
done
