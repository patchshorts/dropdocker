#!/bin/bash
for cert in `/usr/bin/puppet cert list | grep $1 | /bin/grep SHA256 | /usr/bin/awk '{print $1}'`
do
	/usr/bin/puppet cert --sign $1
done
