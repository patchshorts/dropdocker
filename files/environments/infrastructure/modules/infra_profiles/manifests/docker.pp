class infra_profiles::docker inherits infra_profiles {

  $tcp_bind	= hiera('infra::docker::tcpbind','127.0.0.1')
  $socket_bind	= hiera('infra::docker::socket','unix:///var/run/docker.sock')
  $version	= hiera('infra::docker::version',latest)
  $docker_users	= hiera('infra::docker::users',['docker'])
  $dns		= hiera('infra::docker::dns','8.8.8.8')
  $networks	= hiera('infra::docker::net',{})
  $iptables	= hiera('infra::docker::iptables',false)

  # Install docker
  class {'docker':
    version      => $version,
    docker_users => $docker_users,
    use_upstream_package_source => false,
    iptables => $iptables,
  }
  # function call with lambda:
  $networks.each |$value| {
    docker_network { $value[1]["name"]:
      ensure   => present,
      driver   => $value[1]["driver"],
      subnet   => $value[1]["subnet"],
      gateway  => $value[1]["gateway"],
      ip_range => $value[1]["ip_range"],
    }
  }

  include infra_profiles::docker::containers
}
