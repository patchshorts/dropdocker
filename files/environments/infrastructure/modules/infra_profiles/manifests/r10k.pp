class infra_profiles::r10k inherits infra_profiles {

  Package {
    allow_virtual => true,
  }
  package { 'rubygems':
    ensure => present,
  }
  class { 'r10k':
    sources => hiera('infra::r10k::sources',{}),
    require => Package['rubygems'],
  }
}
