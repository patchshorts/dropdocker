class infra_profiles::docker::images inherits infra_profiles::docker {
  $images = hiera('infra::docker::images',{})
  $images.each |$image| {
    $imagename = $image[0]

    docker::image { $imagename:
      ensure    => 'present',
      image_tag => $image[1]["image_tag"],
      force     => $image[1]["force"],
      # docker_file => $image[1]["docker_file"],
      docker_dir => $image[1]["docker_dir"],
      subscribe => File[$image[1]["docker_file"]],
    }

    file { $image[1]["docker_file"]:
      ensure => file,
      content => template('infra_profiles/Dockerfile.erb'),
    }
  }
  file { '/var/lib/docker/scripts/start_puppet.sh':
    ensure => file,
    source => 'puppet:///modules/infra_profiles/start_puppet.sh',
    mode => '0755',
  }
}
