class infra_profiles::docker::containers inherits infra_profiles::docker {

  # Load container definitions
  $containers = hiera('infra::docker::containers',{})
  
  infra_profiles::docker::clean {'clean all container services':
  }

  file { '/var/lib/docker/puppetssl':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0771',
  }
  $containers.each |$container| {
    $containertype = $container[0]

    include infra_profiles::docker::images

    # Define the first ip
    $networks = hiera('infra::docker::net')
    $ipaddress = $container[1]["ip_range"]
    $domain = hiera('infra::hosts::domain')

    #0-3 is actually 4 itterations, but give it a zero and you won't get one iteration
    $realinstances = $container[1]["instances"]
    if $realinstances > 1 {
      $instances = $container[1]["instances"]-1
    } else {
      $instances = $realinstances
    }

    # Spawn number of instances
    $instances.each |$index|{
      alert("instanceeach:${index}")
      # we want to spawn web1-3 not web0-2
      $ourindex = $index+1

      # Set the hostname and ip
      $hostname = "${container[1]["hostnameprefix"]}${ourindex}"
      $ipaddress = ip_address(ip_increment($ipaddress,$ourindex)) # Puppet and SSL require us to set the ip for docker

      # Ofset and increment the host port
      $ports = $container[1]["ports"].map |$port| {
        $hostport = $port + $ourindex - 1 + $container[1]["portoffset"]
        "${hostport}:${port}"
      }

      # Add the ip to the hosts file
      # Auto create a vip and iptables forward the port to that vip: virtual ip
      host { "${hostname}.${domain}":
        ensure       => present,
        host_aliases => ["${hostname}"],
        ip           => $ipaddress,
      }

      # Spawn the container
      docker::run { "${containertype}${ourindex}":
        image           => $container[1]["image"],
        command         => $container[1]["command"],
        ports           => $ports,
        expose          => $container[1]["expose"],
        net             => $container[1]["net"],
        volumes         => $container[1]["volumes"],
        memory_limit    => $container[1]["memory_limit"],
        cpuset          => $container[1]["cpuset"],
        username        => $container[1]["username"],
        hostname        => "${hostname}",
        ipaddress       => "${ipaddress}",
        dns             => $container[1]["dns"],
        restart_service => $container[1]["restart_service"],
        privileged      => $container[1]["privileged"],
        pull_on_start   => $container[1]["pull_on_start"],
        hostentries     => $container[1]["hostentries"],
        extra_parameters => $container[1]["extra_parrameters"],
      }
    }
  }
}
