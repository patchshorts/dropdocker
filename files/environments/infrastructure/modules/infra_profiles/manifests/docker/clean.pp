define infra_profiles::docker::clean ($servicehost="*") {
  exec{'/bin/systemctl reset-failed':
	command => "/bin/systemctl reset-failed"
  }
  exec{'/bin/systemctl daemon-reload':
	command => "/bin/systemctl daemon-reload"
  }
}
