class infra_profiles::aptmirror::apache inherits infra_profiles::aptmirror {
  class { 'apache':
    default_vhost => false,
  }
  apache::vhost { "${::fqdn}":
    port    => '80',
    docroot => '/var/spool/apt-mirror/mirror/archive.ubuntu.com',
    directories => [
        {
            'path' => '/var/spool/apt-mirror/mirror/archive.ubuntu.com',
            'options' => 'Indexes FollowSymLinks MultiViews',
            'allowoverride' => 'Allow All',
        },
    ],
  }
}
