class infra_profiles::aptmirror::mirror inherits infra_profiles::aptmirror {
  package{'apt-mirror':
    ensure => latest,
  }
  file {'/etc/apt/mirror.list':
    ensure => file,
    content => template('infra_profiles/mirror.list.erb'),
  }
}
