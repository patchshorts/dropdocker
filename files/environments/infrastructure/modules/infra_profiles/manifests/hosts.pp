class infra_profiles::hosts inherits infra_profiles {

  $hosts = hiera('infra::hosts::entries',{})
  $purge = hiera('infra::hosts::purge',false)

  class{ 'hosts':
    purge_hosts => $purge,
    host_entries => $hosts,
  }
}
