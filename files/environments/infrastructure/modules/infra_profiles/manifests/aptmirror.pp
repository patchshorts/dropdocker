class infra_profiles::aptmirror inherits infra_profiles {
  include infra_profiles::aptmirror::mirror
  include infra_profiles::aptmirror::apache
}
