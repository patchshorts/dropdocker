class infra_profiles::puppetmaster {

  # Load container definitions
  $containers = hiera('infra::docker::containers',{})

  file { '/var/lib/docker/scripts/sign_cert.sh':
    ensure => file,
    source => 'puppet:///modules/infra_profiles/sign_cert.sh',
    mode => '0755',
  }

  $containers.each |$container| {
    $containertype = $container[0]
    $domain = hiera('infra::hosts::domain')

    #0-3 is actually 4 itterations, but give it a zero and you won't get one iteration
    $realinstances = $container[1]["instances"]
    if $realinstances > 1 {
      $instances = $container[1]["instances"]-1
    } else {
      $instances = $realinstances
    }
    $instances.each |$index|{
      $ourindex=$index+1
      $hostname = "${container[1]["hostnameprefix"]}${ourindex}"
      exec {"/var/lib/docker/scripts/sign_cert.sh ${hostname}":
	command => "/var/lib/docker/scripts/sign_cert.sh ${hostname}",
      }
    }
  }
  include infra_profiles::r10k
}
