node /^.*(?i:cloud)\d+/ {
        include infra_roles::cloud
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node /^.*(?i:puppet)\d+/ {
        include infra_roles::puppetmaster
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node /^.*(?i:docker)\d+/ {
        include infra_roles::docker
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node /^.*(?i:balancer)\d+/ {
        include infra_roles::balancer
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node /^.*(?i:portal)\d+/ {
        include infra_roles::portal
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node default {
  hiera_include('roles','')
  hiera_include($::hostname, [])
}
