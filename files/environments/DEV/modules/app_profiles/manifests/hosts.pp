class tb_profiles::hosts inherits tb_profiles {

  $hosts = hiera('hosts::entries',{})
  $purge = hiera('hosts::purge',false)

  class{ 'hosts':
    purge_hosts => $purge,
    host_entries => $hosts,
  }
}
