# Class: app_roles::web
#
# Puppermaster, libvirt, jenkins server
# Contains:
#   - Virtual Machine Host
#   - Puppetmaster configuration
#   - Jenkins Server
#
class app_roles::web inherits app_roles {
  include tb_profiles::apache
}
