node /^.*(?i:db|sql)\d+/ {
        include tb_roles::db
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node /^.*(?i:app|flask|node)\d+/ {
        include tb_roles::app
        hiera_include('roles','')
        hiera_include($::hostname, [])
}


node /^.*(?i:web|apache|ngin)\d+/ {
        include tb_roles::web
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node /^.*(?i:nfs|store)\d+/ {
        include tb_roles::nfs
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node /^.*(?i:tivoli|nagios|mon)\d+/ {
        include tb_roles::tivolisrv
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node /^.*(?i:netbck|backup)\d+/ {
        include tb_roles::netbckup
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node /^.*(?i:netmon)\d+/ {
        include tb_roles::monsrv
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node /^.*(?i:redis)\d+/ {
        include tb_roles::redis
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node /^.*(?i:gw)\d+/ {
        include tb_roles::gateway
        hiera_include('roles','')
        hiera_include($::hostname, [])
}

node default {
  hiera_include('roles','')
  hiera_include($::hostname, [])
}
